using System;
public class Test{
    public static void Main(string[] args)
    {
        TestCarDetails();
    }

    static void TestCarDetails(){
        Car test = new Car("Ford", "Model T", "BM 18 AP", 1908, 10950);
        Console.WriteLine(test.ToString());
    }
}

public class Car{
    private string make;
    private string model;
    private string registration;
    private int yearOfReg;
    private float currentValue;

    public Car(string make, string model, 
    string registration, int yearOfReg, float currentValue) 
    {
        this.make = make;
        this.model = model;
        this.registration = registration;
        this.yearOfReg = yearOfReg;
        this.currentValue = currentValue;
    }

    public float GetCurrentValue(){
        return currentValue;
    }

    public int GetYearOfReg(){
        return yearOfReg;
    }

    public override string ToString()
    {
        string retStr = "";
        retStr += "Make: " + make; 
        retStr += ", ";
        retStr += "Model: " + model;
        retStr += ", ";
        retStr += "Registration: " + registration;
        retStr += ", ";
        retStr += "Year of Initial Registration: " + yearOfReg;
        retStr += ", ";
        retStr += "Current Value: " + currentValue;
        return retStr;
    }
}
